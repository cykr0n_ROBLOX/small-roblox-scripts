--game.ServerScriptServices.Script

local Players = game:GetService("Players");
local funcSettings = {
	['DeletePackages'] = false; 
	['HighlightEntireBody'] = false;
	['HighlightBodyParts'] = true;
	['RemoveHats'] = true;
};
Players.PlayerAdded:Connect(function(player)
	player.CharacterAppearanceLoaded:Connect(function(character)
		if(funcSettings.RemoveHats) then
			local humanoid = character:WaitForChild("Humanoid");
			humanoid:RemoveAccessories();
		end;
		local detectedPackage, part = false, '';
 		for i,v in pairs(workspace[player.Name]:GetChildren()) do
			if(funcSettings.HighlightBodyParts) then
				if(v.ClassName == 'CharacterMesh') then
					local bp = tostring(v.BodyPart):sub(15);
					if(bp:sub(1,1)=='R') then
						part = bp:sub(1,5)..' '..bp:sub(6);
					elseif(bp:sub(1,1)=='L') then
						part = bp:sub(1,4)..' '..bp:sub(5);
					else
						part = 'Torso';
					end;
					part = v.Parent[part];
				elseif(v.Name == 'Head') then
					if(v.Mesh.MeshType == Enum.MeshType.FileMesh) then
						part = v;
					end;
				end;
				local selectionBox = Instance.new("SelectionBox");
				selectionBox.Adornee = part;
				selectionBox.Color = player.TeamColor;
				selectionBox.Parent = part;
				selectionBox.LineThickness = 0.10;
			elseif(funcSettings.HighlightEntireBody) then
				if(v.ClassName == 'CharacterMesh') then	
					detectedPackage = true;
				elseif(v.Name == 'Head') then
					if(v.Mesh.MeshType == Enum.MeshType.FileMesh) then
						detectedPackage = true;
					end;
				end;
			end;
			if(funcSettings.DeletePackages) then
				if(v.ClassName == 'CharacterMesh') then
					v:Destroy();
				elseif(v.Name == 'Head') then
					v:FindFirstChild('Mesh'):Destroy();
					local newHeadMesh = script.Mesh:Clone();
					newHeadMesh.Parent = v;
				end;
			end;
		end;
		if(funcSettings.HighlightEntireBody) then
			if(detectedPackage) then
				for _,v in pairs({'Head','Right Leg','Left Leg','Right Arm','Left Arm','Torso'}) do
					local selectionBox = Instance.new("SelectionBox");
					selectionBox.Adornee = workspace[player.Name][v];
					selectionBox.Color = player.TeamColor;
					selectionBox.Parent = workspace[player.Name][v];
					selectionBox.LineThickness = 0.10;
				end;
			end;
		end;
	end);
end);

--[[ Note: Allows for removal of packages and replacement with normal parts (Including a normal head mesh which is inside is script but 
        not available here), highlighting with a selectionbox of package parts, or highlighting of entire body hitbox if wearing any package parts.
    The last three settings should probably be one string variable, not three booleans, as I dont think you would want to do more than just one operation.
]]--